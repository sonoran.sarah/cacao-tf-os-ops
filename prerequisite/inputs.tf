variable "username" {
  type        = string
  description = "name of user"
}

variable "external_network_uuid" {
  type        = string
  description = "uuid of the external network"
}

variable "tenant_cidr" {
  type        = string
  description = "private cidr to use for tenant network"
  default     = "10.0.0.0/24"
}

variable "port_rules" {
  type = list(object({
    name             = string
    direction        = string
    ethertype        = string
    protocol         = string
    port_range_min   = number
    port_range_max   = number
    remote_ip_prefix = string
  }))
  description = "set of ports rules"
  default = [
    {
      name             = "tcp22open"
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 22
      port_range_max   = 22
      remote_ip_prefix = "0.0.0.0/0"
    },
    {
      name             = "tcp80open"
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 80
      port_range_max   = 80
      remote_ip_prefix = "0.0.0.0/0"
    },
    {
      name             = "tcp443open"
      direction        = "ingress"
      ethertype        = "IPv4"
      protocol         = "tcp"
      port_range_min   = 443
      port_range_max   = 443
      remote_ip_prefix = "0.0.0.0/0"
    }
  ]
}

variable "keypair_name" {
  type        = string
  description = "name of the keypair"
  default     = "cacao-ssh-key"
}

variable "public_ssh_key" {
  type        = string
  description = "public ssh key to use to create keypair"
}
