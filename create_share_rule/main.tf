terraform {
  required_providers {
    openstack = {
      source = "terraform.cyverse.org/cyverse/openstack"
    }
  }
}

data "openstack_compute_instance_v2" "instance_info" {
  id = var.instance_uuid
}

resource "openstack_sharedfilesystem_share_access_v2" "share_access_01" {
  share_id     = var.share_id
  access_type  = "ip"
  access_to    = data.openstack_compute_instance_v2.instance_info.network[1].fixed_ip_v4
  access_level = "rw"
}
