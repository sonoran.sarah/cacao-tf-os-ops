
variable "instance_uuid" {
  type = string
  description = "uuid of the instance getting a share"
}

variable "share_uuid" {
  type = string
  description = "uuid of the share the rule is affecting"
}
