terraform {
  required_providers {
    openstack = {
      source = "terraform.cyverse.org/cyverse/openstack"
    }
  }
}

resource "openstack_sharedfilesystem_share_v2" "share_01" {
  name             = var.share_name
  description      = var.share_description
  share_proto      = var.share_protocol
  size             = var.share_size
  share_type       = var.share_type
}
