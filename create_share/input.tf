variable "share_name" {
  type = string
  description = "name of share"
}

variable "share_description" {
  type = string
  description = "description of the share"
  default = "this is a test share and can be deleted"
}

variable "share_protocol" {
  type = string
  description = "protocol of the share.  jetstream only supports nfs"
  default = "NFS"
}

variable "share_type" {
  type = string
  description = "type of share.  jetstream only supports cephnfstype"
  default = "cephfsnfstype"
}

variable "share_size" {
  type = string
  description = "size of the share we're creating"
  default = "10"
}
