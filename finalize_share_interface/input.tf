
variable "instance_uuid" {
  type = string
  description = "uuid of the instance getting a share"
}
