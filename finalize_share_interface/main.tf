terraform {
  required_providers {
    openstack = {
      source = "terraform.cyverse.org/cyverse/openstack"
    }
  }
}

data "openstack_compute_instance_v2" "instance_info" {
  id = var.instance_uuid
}

# todo: find a way to run this ansible-playbook command:
# ansible-playbook -i hosts.yml playbook.yml --extra-vars "secondary_interface_ip=${data.openstack_compute_instance_v2.instance_info.network[1].fixed_ip_v4}"

# it should look like this:
# ansible-playbook -i hosts.yml playbook.yml --extra-vars "secondary_interface_ip=10.255.0.53/16"

# reading this: https://alex.dzyoba.com/blog/terraform-ansible/ suggests provisioners,
# but reading this: https://www.terraform.io/docs/language/resources/provisioners/syntax.html suggests provisioners _have to_ be nested inside resource stanzas

#provisioner "local-exec" {
#  command = "ansible-playbook -i hosts.yml playbook.yml --extra-vars \"secondary_interface_ip=${data.openstack_compute_instance_v2.instancestuff.network[1].fixed_ip_v4}\""
#}
